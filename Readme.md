#**Chuyên Đề Mobile &pervasive computing**<br/>
#**Ứng dụng nghe nhạc trực tuyến MusicOnline**<br/>
#**Thành viên:**<br/>
**##Nguyễn Văn Tuấn -17521218**<br/>
**##Đặng Đình Quyền Anh - 17520227 **<br/>
#Chức năng: <br/>
Ứng dụng cung cấp các danh sách nhạc đa dạng và sinh động , phù hợp với mọi lứa tuổi:<br/>
*Các bài hát hot hiện nay và luôn tự động cập nhật.<br/>
*Cách danh sách nhạc gợi ý , nhạc việt , nhạc bolero, nhạc Rap, nhạc ...<br/>
*Và các thể loại nhạc nước ngoài US-UK, K-Pop. C-Pop<br/>
*Đăng nhập tài khoản firebase để lưu cá nhân hóa sở thích nghe nhạc của mình, thả yêu thích ,bình luận cảm xúc bài nhạc.<br/>
*Tải nhạc về máy để nghe nhạc offline,tạo playlist yêu thích.<br/>
#môi trường phát triển:Yêu cầu Node 14.x ,react-native 0.61, pypthon2,3.
#Yêu cầu thiết bị: Thiết bị di động hệ điều hành android 7.0 đến android 10.**(Android 11 bị lỗi đọc ghi file nên không dùng được)**.<br/>
#Thư viện và công nghệ: React native, redux. <br/>
#Backend/Database/Diagram: Firebase<br/>
#Tác giả: Nguyễn Văn Tuấn & Đặng Đình Quyền Anh<br/>
#Giấy phép: Không có <br/>
#Màn hình:<br/>
1: Màn đăng nhập:<br/>
![dangnhap](/uploads/3e07df3768bddea798144a7bc917eed5/dangnhap.png)
![quenmk](/uploads/660b8b6f99807c9e9e362229a4223180/quenmk.png)
2 Màn hình chính:
![nhchinh](/uploads/e7663da8df7128376a25a2f4fce4b383/nhchinh.png)
![mhchinh2](/uploads/383ecc568e2ff3acc7a98951e16c225f/mhchinh2.png)
![mhchinh3](/uploads/10303446374ef0f38d9f6be114e5117d/mhchinh3.png)
3 Màn hình đang phát:
![nowplaying](/uploads/107b13b2cb89ce1fa588f4fbb4eec3e3/nowplaying.png)
![play](/uploads/8d6ff52174f64ab675b60346306048da/play.png)
![lyric](/uploads/448b3716be112e3d3eced942ec0c8ac0/lyric.png)

4 Thư viện:
![library](/uploads/9169264ce5b86900eeec1113fa450f32/library.png)
5 Hồ sơ:
![profile](/uploads/e7ea91da927883ac20d46de54a1f28ea/profile.png)
![profile3](/uploads/e18347aa06a528c674e3bf168bac966c/profile3.png)
