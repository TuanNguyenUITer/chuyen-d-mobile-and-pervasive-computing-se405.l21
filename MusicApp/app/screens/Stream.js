/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Button,
  Switch,
  Slider,
  Share,
  Linking,
  Alert,
  TextInput,
  FlatList,
} from 'react-native';
import Player, {MyLyric, MyPlayerBar} from '../player/Player';
import ReLoadSong from '../components/MyPlayerBar';
import Modal from 'react-native-modal';
import Icon_ from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/FontAwesome';
import ItemComment from '../components/ItemComment';
import DanhSachBaiHat from '../components/DanhSachBaiHat';
import {connect} from 'react-redux';
import {
  setSongPlay,
  setPause,
  setPlay,
  setIndexPlayingInList,
} from '../redux/action';
import {FirebaseApp} from '../components/FirebaseConfig.js';
import RNFetchBlob from 'rn-fetch-blob';

const screenWidth = Math.round(Dimensions.get('window').width);

class StreamScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pause: false,
      repeat: false,
      random: false,
      rerender: true,
      margindelay_: screenWidth / 2,
      toLeft: true,
      nameSong: '',

      popUpCmt: false,
      dataCmt: [],
      valueCmt: '',
      popUpClock: false,
      popUpChooseTheme: false,
      clock_On: false,
      timerValue: 0,
      popUpSpeed: false,
      speedValue: 1,
      typeLoop: 0,
      typeNext: 0,
      theme: 0,
      nowPlay: {},
    };
  }
  static navigationOptions = {
    header: null,
  };

  _setPause() {
    if (this.props.isPause === true) {
      //this.setState({pause: false});
      this.props.setPause();
      Player._setPause();
    } else {
      //this.setState({pause: true});
      this.props.setPlay();
      Player._setPlay();
    }
    console.log(this.state.pause);
  }

  _renderButtonPause() {
    if (this.props.isPause === true)
      return <Icon color="red" name={'pause'} size={70} />;
    else return <Icon color="red" name={'pause'} size={70} />;
  }

  _sendCmt() {
    if (this.state.valueCmt === '') return;
    if (this.props.myCurrentSong.id == null) return;
    let t = new Date();
    if (FirebaseApp.auth().currentUser == null) {
      Alert.alert('Vui lòng đăng nhập để bình luận!');
      return;
    }
    if (FirebaseApp.auth().currentUser.displayName == null) {
      Alert.alert('Vui lòng cập nhật hồ sơ cá nhân để bình luận!');
      return;
    }
    FirebaseApp.database()
      .ref('Comment/' + this.props.myCurrentSong.id)
      .push({
        name: FirebaseApp.auth().currentUser.displayName,
        cmt: this.state.valueCmt,
        time:
          t.getDate() +
          '/' +
          (t.getMonth() + 1) +
          '/' +
          t.getFullYear() +
          '---' +
          t.getHours() +
          ':' +
          t.getMinutes(),
        uid: FirebaseApp.auth().currentUser.uid,
      });
    this.setState({valueCmt: ''});
  }

  _loadDataCmt(idSong) {
    if (idSong === '' || idSong == null) return;
    var items = [];

    this.setState({dataCmt: items});
    try {
      FirebaseApp.database()
        .ref('Comment/' + idSong)
        .on('child_added', dataSnapshot => {
          var t = dataSnapshot.toJSON();
          items.push(t);
          this.setState({dataCmt: items});
        });
    } catch (error) {
      return;
    }

    for (let i = 0; i < items.length; i++) {
      FirebaseApp.storage()
        .ref('/images/' + items[i].uid + '.jpg')
        .getDownloadURL()
        .catch(function(error) {})
        .then(URL => {
          //var t = dataSnapshot.toJSON();
          if (URL == null) {
            items[i].image = null;
            //console.log(url);
          } else {
            items[i].image = URL;
          }
        })
        .then(() => {
          this.setState({dataCmt: items});
        });
    }
  }

  _loadTheme() {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/DataLocal/theme';
    RNFetchBlob.fs.exists(path).then(value => {
      if (!value) {
        fs.mkdir(path).then(() => {
          fs.createFile(path + '/theme.txt', '1', 'utf8');
        });
      } else {
        fs.readFile(path + '/theme.txt', 'utf8').then(value => {
          this.setState({theme: value});
        });
      }
    });
  }

  _setTheme(value) {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/DataLocal/theme/theme.txt';
    fs.writeFile(path, value + '', 'utf8');
    console.log('set thêm ' + value);
  }

  componentDidMount() {
    //this._loadDataCmt()
    //this._loadDataCmt(this.props.myCurrentSong.id)s
    console.log(this.props.myCurrentSong);
  }

  static _on = false;
  ShowAlertWithDelay = () => {
    setTimeout(function() {
      //Put All Your Code Here, Which You Want To Execute After Some Delay Time.

      //this.setState({clock_On:false})

      StreamScreen._on = true;
      Player._setPause();
      Alert.alert('Đã tắt');
    }, this.state.timerValue * 1000 * 60);
  };

  openLink() {
    var name = this.change_alias(this.props.myCurrentSong.title);
    var art = this.change_alias(this.props.myCurrentSong.artists_names);
    var url =
      'https://zingmp3.vn/bai-hat/' +
      name +
      '-' +
      art +
      '/' +
      this.props.myCurrentSong.id +
      '.html';
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  _shareSong() {
    var name = this.change_alias(this.props.myCurrentSong.title);
    var art = this.change_alias(this.props.myCurrentSong.artists_names);
    var link =
      'https://zingmp3.vn/bai-hat/' +
      name +
      '-' +
      art +
      '/' +
      this.props.myCurrentSong.id +
      '.html';
    console.log();
    Share.share(
      {
        message: this.props.myCurrentSong.title + ' ' + link,
        url: link,
        title: 'Wow, did you see that?',
      },
      {
        // Android only:
        dialogTitle: 'Share ' + this.props.myCurrentSong.title,
        // iOS only:
        excludedActivityTypes: ['com.apple.UIKit.activity.PostToTwitter'],
      },
    );
  }

  change_alias(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    str = str.replace(/,/g, '');
    str = str.replace(/ /g, '-');
    return str;
  }
  updateTheme = theme => {
    this.setState({theme: theme});
    this._setTheme(theme);
  };
  render() {
    var data = [1, 2, 3, 4, 5];

    if (StreamScreen._on === true) {
      this.setState({clock_On: false, timerValue: 0});
      this.props.setPause();
      StreamScreen._on = false;
    }
    const colorButton = '#000000';
    return (
      <View style={{width: '100%', height: '100%'}}>
        <View style={[styles.container, {backgroundColor: 'transparent'}]}>
          <View style={[styles.con2, {position: 'relative'}]}>
            {/*Ten bai hat  */}
            <View
              style={{
                backgroundColor: '#0abde395',
                flexDirection: 'row',
                justifyContent: 'center',
                height: 40,
              }}>
              <Text style={styles.tieuDe}>Danh sách đang phát</Text>
            </View>

            <ScrollView
              horizontal={true}
              pagingEnabled={true}
              ref={myScroll => (this._myScroll = myScroll)}
              showsHorizontalScrollIndicator={true}>
              {/* Man hinh loi */}
              <View style={{flex: 1, width: screenWidth}}>
                <View
                  style={{
                    margin: 10,
                    flex: 1.2,
                    alignItems: 'center',
                    borderWidth: 0,
                    borderColor: '#999',
                    borderRadius: 5,
                    width: '95%',
                    padding: 5,
                  }}>
                  <Image
                    style={{
                      marginTop: 30,
                      width: 300,
                      height: 300,
                      borderRadius: 30,
                      backgroundColor: '#81ecec',
                      borderColor: '#0984e3',
                      borderWidth: 2,
                    }}
                    resizeMode={'cover'}
                    source={
                      this.props.myCurrentSong.image === undefined ||
                      this.props.myCurrentSong.image === ''
                        ? require('../../res/disk1.png')
                        : {uri: this.props.myCurrentSong.image}
                    }
                    //source={require('../../res/disk1.png')}
                  />
                  <View
                    style={[
                      styles.con2,
                      {position: 'relative', marginTop: 10},
                    ]}>
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#341f97',
                        textAlign: 'center',
                        fontWeight: 'bold',
                        paddingLeft: 5,
                      }}>
                      {this.props.myCurrentSong.title}
                    </Text>
                    <Text
                      style={{
                        fontSize: 12,
                        color: '#341f97',
                        textAlign: 'center',
                        paddingLeft: 5,
                      }}>
                      {this.props.myCurrentSong.artists_names}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{flex: 1, width: screenWidth}}
                backgroundColor="transparent">
                {/* <Text>{this.props.myCurrentSong.lyric}aaa</Text> */}

                <MyLyric linkLyric={this.props.myCurrentSong.lyric} />
              </View>

              {/*Man hinh khac  */}
              <View
                style={{
                  flex: 1,
                  width: screenWidth,
                  backgroundColor: 'transparent',
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    width: screenWidth,
                    marginLeft: 10,
                    //width: screenWidth,

                    zIndex: 9,
                  }}>
                  <DanhSachBaiHat
                    kind={'BHDN'}
                    canRemove={false}
                    dataDanhSachBaiHat={
                      this.props.dataDanhSachDangNghe.dataSong
                    }
                    isTrongSuot="true"
                  />
                </View>
              </View>
            </ScrollView>
          </View>

          {/* Phan thanh player */}
          <View style={[styles.con1, {backgroundColor: '48dbfb33'}]}>
            <View style={{flexDirection: 'row', padding: 3, marginBottom: 5}}>
              <View
                style={{
                  padding: 2,
                  paddingRight: 9,
                  paddingLeft: 9,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this._loadDataCmt(this.props.myCurrentSong.id);
                    this.setState({popUpCmt: true});
                  }}>
                  <Icon_ name="comments" color={colorButton} size={25} />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  padding: 2,
                  paddingRight: 9,
                  paddingLeft: 9,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({popUpSpeed: true});
                  }}>
                  <Icon_ name="tachometer-alt" color={colorButton} size={25} />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  padding: 2,
                  paddingRight: 9,
                  paddingLeft: 9,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this._myScroll.scrollToEnd();
                  }}>
                  <Icon_ name="list-alt" color={colorButton} size={25} />
                </TouchableOpacity>
                {/* <Text style={{color:"#FFF"}}>List song</Text> */}
              </View>

              <View
                style={{
                  padding: 2,
                  paddingRight: 9,
                  paddingLeft: 9,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this._shareSong();
                  }}>
                  <Icon_
                    name="share-alt-square"
                    color={colorButton}
                    size={25}
                  />
                </TouchableOpacity>
                {/* <Text style={{color:"#FFF"}}>Share</Text> */}
              </View>

              <View
                style={{
                  padding: 2,
                  paddingRight: 9,
                  paddingLeft: 9,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.openLink();
                  }}>
                  <Icon_ name="info-circle" color={colorButton} size={25} />
                </TouchableOpacity>
                {/* <Text style={{color:"#FFF"}}>Infomation</Text> */}
              </View>
            </View>

            <ReLoadSong index={this.props.indexPlayingInList} />

            <MyPlayerBar />

            <View
              style={[
                styles.containerButton,
                {backgroundColor: 'transparent'},
              ]}>
              <Text />
              <View flex={1} style={{marginLeft: 20, paddingRight: 10}}>
                <TouchableOpacity
                  flex={1}
                  onPress={() => {
                    Player.NextTypeNext();
                    this.setState({typeNext: Player.typeNext});
                  }}>
                  {/* <Image
                    style={{height: '100%'}}
                    resizeMode="center"
                    color={colorButton}
                    source={
                      this.state.typeNext === 0
                        ? require('../../res/shuffle.png')
                        : require('../../res/shuffle_selected.png')
                    }
                  /> */}
                  <Icon name="random" size={40} color={colorButton} />
                </TouchableOpacity>
              </View>

              <View flex={1}>
                <TouchableOpacity
                  flex={1}
                  onPress={() => {
                    console.log('pre');
                    Player.PlayPre();
                    Player._setPlay();
                  }}>
                  <Icon color={colorButton} name="backward" size={40} />
                  {/* <Image
                    style={{height: '83%', marginTop: 5, marginBottom: 5}}
                    resizeMode="center"
                    source={require('../../res/prev.png')}
                  /> */}
                </TouchableOpacity>
              </View>

              <View flex={1}>
                <TouchableOpacity
                  onPress={() => {
                    this._setPause();
                  }}>
                  {/* {this._renderButtonPause()} */}
                  {this.props.isPause === true ? (
                    <Icon color={colorButton} name={'pause'} size={60} />
                  ) : (
                    <Icon color={colorButton} name={'play'} size={60} />
                  )}
                </TouchableOpacity>
              </View>

              <View flex={1}>
                <TouchableOpacity
                  onPress={() => {
                    //console.log('Next');
                    Player.PlayNext();
                    Player._setPlay();
                  }}>
                  <Icon color={colorButton} name="forward" size={40} />
                </TouchableOpacity>
              </View>

              <View flex={1}>
                <TouchableOpacity
                  onPress={() => {
                    Player.NextTypeLoop();
                    this.setState({typeLoop: Player.typeLoop});
                  }}>
                  <Icon
                    color={colorButton}
                    name="repeat"
                    size={30}
                    style={{marginTop: 5, marginBottom: 5}}
                  />
                  {this.state.typeLoop === 0 ? (
                    <Text style={{fontSize: 8, marginLeft: 0}}>Repeat</Text>
                  ) : this.state.typeLoop === 1 ? (
                    <Text style={{fontSize: 8, marginLeft: 7}}>All</Text>
                  ) : (
                    <Text style={{fontSize: 8, marginLeft: 5}}>One</Text>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            //marginBottom: -30,
          }}>
          <Modal
            visible={this.state.popUpCmt}
            transparent={true}
            animationType="slide"
            onBackdropPress={() => {
              this.setState({popUpCmt: false});
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 20,

                height: 550,
                width: 350,
                backgroundColor: '#ffffffec',
                //justifyContent: 'center',
              }}>
              <Button title={this.props.myCurrentSong.title} />
              <Text />
              <View
                style={{
                  flex: 7,
                  backgroundColor: '#ffffffdd',
                  marginBottom: 10,
                  borderRadius: 10,
                }}>
                {}
                <FlatList
                  data={this.state.dataCmt}
                  extraData={this.state.dataCmt}
                  renderItem={({item, index}) => (
                    <ItemComment
                      urlImage={
                        item.image == null
                          ? 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png'
                          : item.image
                      }
                      name={item.name}
                      time={item.time}
                      cmt={item.cmt}
                    />
                  )}
                />
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 30,
                }}>
                <View style={{flexDirection: 'row', marginLeft: 0}}>
                  <TextInput
                    placeholder="Nhập bình luận"
                    value={this.state.valueCmt}
                    onChangeText={text => {
                      this.setState({valueCmt: text});
                    }}
                    style={{
                      backgroundColor: '#FFB6C1',
                      width: 260,
                      height: 45,
                      borderRadius: 5,
                    }}
                  />
                  <TouchableOpacity
                    style={{padding: 5}}
                    onPress={() => {
                      this._sendCmt();
                    }}>
                    <Icon name="send" size={25} color="#81ecec" />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <Modal
            visible={this.state.popUpClock}
            transparent={true}
            animationType="slide"
            style={{alignItems: 'center'}}
            onBackdropPress={() => {
              this.setState({popUpClock: false});
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 20,
                // marginTop: -50,
                height: 150,
                width: 150,
                backgroundColor: '#fffffffa',
                alignItems: 'center',
                //justifyContent: 'center',
              }}>
              <Button title={'Hẹn giờ'} />
              <Text />

              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', marginLeft: 0}}>
                  <TextInput
                    keyboardType="numeric"
                    maxLength={3}
                    value={this.state.timerValue}
                    onChangeText={text => {
                      this.setState({timerValue: text});
                    }}
                    style={{
                      backgroundColor: '#FFFF',
                      width: 80,
                      height: 35,
                      borderRadius: 5,
                    }}
                  />
                  <Text style={{color: 'gray'}}> phút</Text>
                  {/* <Button title={this.state.clock_On==false? 'Tắt':"Bật"} onPress={()=>{this.setState({clock_On:!this.state.clock_On});
                  if(this.state.clock_On)
                  {
                    this.ShowAlertWithDelay();
                  }
                }}></Button> */}

                  {/* <TouchableOpacity
                    onPress={() => {
                      this._sendCmt();
                    }}>
                    <Icon name="send" size={25} color="#81ecec"></Icon>
                  </TouchableOpacity> */}
                </View>
                <Switch
                  value={this.state.clock_On}
                  onTouchEnd={() => {
                    this.setState({clock_On: !this.state.clock_On});
                    if (!this.state.clock_On && this.state.timerValue != 0) {
                      this.ShowAlertWithDelay();
                    }
                  }}
                />
                {/* <Text>Báo thức đang: {this.state.clock_On==false? 'Tắt':"Bật"} </Text> */}
              </View>
            </View>
          </Modal>

          <Modal
            visible={this.state.popUpChooseTheme}
            transparent={true}
            animationType="slide"
            style={{alignItems: 'center'}}
            onBackdropPress={() => {
              this.setState({popUpChooseTheme: false});
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 20,
                // marginTop: -50,
                height: 350,
                width: 280,
                backgroundColor: '#fffffffa',
                // alignItems: 'center',
                //justifyContent: 'center',
              }}>
              <Button title={'Chọn hình nền'} />
              <Text />

              <FlatList
                data={data}
                horizontal={true}
                renderItem={({item, index}) => (
                  <View style={{flex: 1, margin: 10}}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({theme: item});
                        this.updateTheme(item);
                      }}>
                      <Image
                        style={{width: 100, height: 150}}
                        resizeMode="stretch"
                        source={
                          item == 1
                            ? require('../../res/BGStream1.jpg')
                            : item == 2
                            ? require('../../res/BG2.jpg')
                            : item == 3
                            ? require('../../res/BG3.jpg')
                            : item == 4
                            ? require('../../res/BG4.jpg')
                            : item == 5
                            ? require('../../res/BG5.jpg')
                            : item == 6
                            ? require('../../res/BG6.jpg')
                            : item == 7
                            ? require('../../res/BG7.jpg')
                            : item == 8
                            ? require('../../res/BG8.jpg')
                            : item == 9
                            ? require('../../res/BG9.jpg')
                            : item == 10
                            ? require('../../res/BG10.jpg')
                            : require('../../res/BGStream1.jpg')
                        }
                      />
                    </TouchableOpacity>
                  </View>
                )}
              />

              <FlatList
                data={data}
                horizontal={true}
                renderItem={({item, index}) => (
                  <View style={{flex: 1, margin: 10}}>
                    <TouchableOpacity
                      onPress={() => {
                        {
                          this.setState({theme: item + 5});
                        }
                        this.updateTheme(item + 5);
                      }}>
                      <Image
                        style={{width: 100, height: 150}}
                        resizeMode="stretch"
                        source={
                          item + 5 == 1
                            ? require('../../res/BGStream1.jpg')
                            : item + 5 == 2
                            ? require('../../res/BG2.jpg')
                            : item + 5 == 3
                            ? require('../../res/BG3.jpg')
                            : item + 5 == 4
                            ? require('../../res/BG4.jpg')
                            : item + 5 == 5
                            ? require('../../res/BG5.jpg')
                            : item + 5 == 6
                            ? require('../../res/BG6.jpg')
                            : item + 5 == 7
                            ? require('../../res/BG7.jpg')
                            : item + 5 == 8
                            ? require('../../res/BG8.jpg')
                            : item + 5 == 9
                            ? require('../../res/BG9.jpg')
                            : item + 5 == 10
                            ? require('../../res/BG10.jpg')
                            : require('../../res/BGStream1.jpg')
                        }
                      />
                    </TouchableOpacity>
                  </View>
                )}
              />
              {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Picker style={{ height: 50, width: 150 }} mode='dropdown'
                  selectedValue={this.state.theme} onValueChange={this.updateTheme}>
                  
                  <Picker.Item label="Hình nền 1" value={1} />
                  <Picker.Item label="Hình nền 2" value={2} />
                  <Picker.Item label="Hình nền 3" value={3} />
                  <Picker.Item label="Hình nền 4" value={4} />
                  <Picker.Item label="Hình nền 5" value={5} />
                  <Picker.Item label="Hình nền 6" value={6} />
                  <Picker.Item label="Hình nền 7" value={7} />
                  <Picker.Item label="Hình nền 8" value={8} />
                  <Picker.Item label="Hình nền 9" value={9} />
                  <Picker.Item label="Hình nền 10" value={10} />
                </Picker>
              </View> */}
            </View>
          </Modal>

          <Modal
            visible={this.state.popUpSpeed}
            transparent={true}
            animationType="slide"
            style={{alignItems: 'center'}}
            onBackdropPress={() => {
              this.setState({popUpSpeed: false});
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 20,
                // marginTop: -50,
                height: 150,
                width: 250,
                backgroundColor: '#fffffffa',
                alignItems: 'center',
                //justifyContent: 'center',
              }}>
              <Button title={'Tốc độ phát'} />
              <Text />

              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', marginLeft: 0}}>
                  <TextInput
                    keyboardType="numeric"
                    maxLength={4}
                    editable={false}
                    value={this.state.speedValue.toString()}
                    style={{
                      backgroundColor: '#FFFF',
                      width: 35,
                      height: 35,
                      borderRadius: 5,
                    }}
                  />
                  <Slider
                    minimumTrackTintColor="#2e86de"
                    thumbTintColor="#2e86de"
                    width={200}
                    marginBottom={0}
                    maximumValue={2}
                    minimumValue={0.25}
                    step={0.25}
                    value={this.state.speedValue}
                    onSlidingComplete={value => {
                      Player._setRate(value);

                      this.setState({speedValue: value});
                    }}
                  />
                </View>
                {/* <Switch
                  value={this.state.clock_On}
                  onTouchEnd={() => {
                    this.setState({clock_On: !this.state.clock_On});
                    if (!this.state.clock_On && this.state.timerValue != 0) {
                      this.ShowAlertWithDelay();
                    }
                  }}></Switch> */}
                {/* <Text>Báo thức đang: {this.state.clock_On==false? 'Tắt':"Bật"} </Text> */}
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    myCurrentSong: state.currentSong,
    isPause: state.isPause,
    dataDanhSachDangNghe: state.dataDanhSachDangNghe,
    indexPlayingInList: state.indexPlayingInList,
  };
}
export default connect(
  mapStateToProps,
  {
    setSongPlay,
    setPause,
    setPlay,
    setIndexPlayingInList,
  },
)(StreamScreen);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '100%',
  },
  con1: {
    flex: 1.2,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10,
  },
  con2: {
    width: screenWidth,
    flex: 5,
  },

  containerButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: screenWidth,
    marginBottom: 20,
    marginTop: 15,
  },
  containerProc: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    flexDirection: 'row',
    width: screenWidth,
  },
  tieuDe: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    fontSize: 20,
    fontFamily: 'vincHand',
    fontWeight: 'bold',
    marginTop: 3,
    marginLeft: 8,
    marginBottom: 5,
    borderStartWidth: 2,
    borderRightColor: 'red',
    color: '#341f97',
  },
});
