import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DanhSachBaiHat from '../components/DanhSachBaiHat';
const screenWidth = Math.round(Dimensions.get('window').width);

export default class imagplaylist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentWillMount() {
    this._loadData();
  }
  _loadData() {
    let response;
    switch (this.props.navigation.getParam('numb')) {
      case 0:
        response = require('../../res/playlist/workfromhome.json');
        break;
      case 1:
        response = require('../../res/playlist/nhacmoi.json');
        break;
      case 2:
        response = require('../../res/playlist/acoustic.json');
        break;
      case 3:
        response = require('../../res/playlist/vpopsongca.json');
        break;
      default:
        break;
    }
    this.setState({
      data: response.data,
    });
  }
  render() {
    return (
      <View style={styles.container1}>
        <View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Home')}>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <Icon
                name={'arrow-left'}
                color={'#341f97'}
                size={30}
                style={{marginTop: 7}}
              />
              <Text style={styles.tieuDe}> {this.state.data.title}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Image style={styles.img} source={{uri: this.state.data.thumbnailM}} />

        <DanhSachBaiHat
          kind="download"
          dataDanhSachBaiHat={this.state.data.song.items}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  container1: {
    flex: 2,
    width: screenWidth * 0.95,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'column',
    margin: 10,
    borderRadius: 10,
  },
  tieuDe: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    fontSize: 20,
    fontFamily: 'vincHand',
    fontWeight: 'bold',
    marginTop: 3,
    marginLeft: 8,
    marginBottom: 5,
    borderStartWidth: 2,
    borderRightColor: 'red',
    color: '#341f97',
  },
  img: {
    margin: 10,
    width: 350,
    height: 350 * 0.7,
    resizeMode: 'cover',
    borderRadius: 10,
  },
});
