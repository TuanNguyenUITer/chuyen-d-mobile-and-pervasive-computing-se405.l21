import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  PermissionsAndroid,
  BackHandler,
} from 'react-native';
//import firebaseApp from 'firebase'
import Icon from 'react-native-vector-icons/FontAwesome5';
import RNFetchBlob from 'rn-fetch-blob';
import {Alert} from 'react-native';
import {FirebaseApp} from '../components/FirebaseConfig.js';
//import {firebaseConfig} from "../components/FirebaseConfig"

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderLogin: true,
      user: '',
      pass: '',
      rePass: '',
      spinner: true,
    };
  }
  componentDidMount() {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/DataLocal/p.txt';
    this.requestCameraPermission().then(() => {
      this.requestReadFilePermission().then(() => {
        this.requestWriteFilePermission();
      });
    });
    this._checkFileLocal();
    FirebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({user: FirebaseApp.auth().currentUser.email});
        fs.readFile(path, 'utf8').then(data => {
          this.setState({pass: data}), console.log(data);
        });
      }
      // this.setState({spinner: false});
    });
  }
  _checkFileLocal() {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/MusicOnline';
    RNFetchBlob.fs.exists(path).then(value => {
      if (!value) {
        fs.mkdir(path)
          .then(() => {
            fs.createFile(path + '/p.txt', '', 'utf8');
            let objTheme = {currentTheme: 1};
            fs.createFile(
              path + '/theme.txt',
              JSON.stringify(objTheme),
              'utf8',
            );
            fs.mkdir(path + '/PlayList_Local').catch(err => {
              console.log('Lỗi ' + err.message);
            });
            fs.mkdir(path + '/BaiHatVuaNghe');
            fs.mkdir(path + '/Music_Local').then(() => {
              fs.mkdir(path + '/Music_Local/DataMusicLocal');
            });
          })
          .then(() => {
            let objMusicLocalManager = {total_song: 0, items: []};
            fs.createFile(
              path + '/Music_Local/MusicLocalManager.js',
              JSON.stringify(objMusicLocalManager),
              'utf8',
            );

            let objPlayList_Local = {total_list: 0, list: []};
            fs.createFile(
              path + '/PlayList_Local/PlayListManager.js',
              JSON.stringify(objPlayList_Local),
              'utf8',
            );

            let objBHVuaNghe = {items: []};
            fs.createFile(
              path + '/BaiHatVuaNghe/BaiHatVuaNghe.js',
              JSON.stringify(objBHVuaNghe),
              'utf8',
            );
          });
      }
    });
  }
  _clearAll() {
    this.setState({user: '', pass: '', rePass: ''});
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          // title: 'Cấp quyền máy ảnh',
          // message:
          //   ' ',
          // buttonNeutral: 'Hỏi lại sau',
          // buttonNegative: 'Hủy',
          // buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      } else {
        //Alert.alert("Không cấp quyền máy ảnh ")
        BackHandler.exitApp();
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async requestReadFilePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          // title: 'Cấp quyền đọc file',
          // message:
          //   ' ',
          // buttonNeutral: 'Hỏi lại sau',
          // buttonNegative: 'Hủy',
          // buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      } else {
        BackHandler.exitApp();
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async requestWriteFilePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          // title: 'Cấp quyền ghi file',
          // message:
          //   ' ',
          // buttonNeutral: 'Hỏi lại sau',
          // buttonNegative: 'Hủy',
          // buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      } else {
        BackHandler.exitApp();
      }
    } catch (err) {
      console.warn(err);
    }
  }

  _renderButton() {
    return (
      <View
        style={{
          // flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'flex-end',
          marginBottom: 10,
        }}>
        <TouchableOpacity
          onPress={() => {
            this._Login();

            this._clearAll();
          }}>
          <View
            style={
              this.state.renderLogin === true
                ? styles.conButon1
                : styles.conButon2
            }>
            <Text> Đăng nhập</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this._Register();
          }}>
          <View style={styles.conButon2}>
            <Text>Đăng kí</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('ForgotPass');
          }}>
          <Text style={{color: '#000'}}>Quên mật khẩu</Text>
        </TouchableOpacity>
      </View>
    );
  }
  _Register() {
    FirebaseApp.auth()
      .createUserWithEmailAndPassword(this.state.user, this.state.pass)
      .then(() => {
        //this.setState({spinner: false});
        Alert.alert('Đăng kí thành công!');
        FirebaseApp.auth().currentUser.sendEmailVerification();
        //this.setState({renderLogin: true});
      })
      .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage + ' code: ' + errorCode);
        if (errorCode == 'auth/email-already-in-use') {
          Alert.alert('E-mail này đã được đăng kí');
          return;
        }

        if (errorCode == 'auth/invalid-email') {
          Alert.alert('E-mail không hợp l!');
          return;
        }

        if (errorCode == 'auth/weak-password') {
          Alert.alert('Mật khẩu phải nhiều hơn 5 kí tự!');
          return;
        }

        // ...
      });
  }

  _Login() {
    var fs = RNFetchBlob.fs;
    var path = RNFetchBlob.fs.dirs.SDCardDir + '/DataLocal/p.txt';

    FirebaseApp.auth()
      .signInWithEmailAndPassword(this.state.user, this.state.pass)
      .then(() => {
        fs.writeFile(path, this.state.pass, 'utf8');

        this.props.navigation.navigate('Home');
      })
      .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage + ' code: ' + errorCode);
        if (errorCode == 'auth/invalid-email') {
          Alert.alert('E-mail không hợp lệ');
          return;
        }
        if (errorCode == 'auth/wrong-password') {
          fs.writeFile(path, '', 'utf8');
          Alert.alert('Sai mật khẩu!');
          return;
        }
        // ...
      });
  }

  _renderRepass() {
    return (
      <View style={styles.conLogin}>
        <Text> </Text>
        <Icon name="lock" size={35} color="#000" />
        <TextInput
          secureTextEntry={true}
          value={this.state.rePass}
          placeholder=" Nhập lại mật khẩu"
          style={styles.conInput}
          onChangeText={text => {
            this.setState({rePass: text});
          }}
        />
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.logo}
            source={{
              uri:
                'https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Muse_Music_App_Logo.png/900px-Muse_Music_App_Logo.png',
            }}
          />
          <Text style={styles.title}>Welcome</Text>
        </View>
        <View style={styles.footer}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              flex: 1,
              marginTop: 75,
            }}>
            <View style={styles.conLogin}>
              <Text> </Text>
              <Icon name="user" size={35} style={styles.icon} color="#000" />

              <Text> </Text>
              <TextInput
                value={this.state.user}
                placeholder="  E-mail"
                style={styles.conInput}
                onChangeText={text => {
                  this.setState({user: text});
                }}
              />
            </View>
            <Text style={{fontSize: 3}} />
            <View style={styles.conLogin}>
              <Text> </Text>
              <Icon name="unlock-alt" size={35} color="#000" />
              <Text> </Text>
              <TextInput
                secureTextEntry={true}
                value={this.state.pass}
                placeholder=" Mật khẩu"
                style={styles.conInput}
                onChangeText={text => {
                  this.setState({pass: text});
                }}
              />
            </View>
            {/* <Text style={{fontSize: 3}} />
            {this.state.renderLogin == false ? this._renderRepass() : null} */}
            <View style={{height: 20}} />
            {this._renderButton()}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Home')}>
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <Text> Bỏ qua đăng nhập</Text>
                <Icon
                  style={{paddingLeft: 10, marginTop: -5}}
                  name={'sign-in-alt'}
                  color={'#341f97'}
                  size={30}>
                  {' '}
                </Icon>
              </View>
            </TouchableOpacity>
          </View>
          <View />
        </View>
      </View>
    );
  }
}

//const {height} = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFB6C1'},
  header: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  footer: {
    flex: 2,
    backgroundColor: '#fff',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  icon: {
    marginTop: 5,
    marginLeft: 5,
  },
  logo: {
    width: 150,
    height: 150,
    resizeMode: 'cover',
    borderRadius: 39,
    justifyContent: 'center',
    alignItems: 'center',
  },
  conLogin: {
    flexDirection: 'row',
    //justifyContent: 'center',
    borderWidth: 2,
    backgroundColor: 'transparent',
    borderRadius: 10,
  },
  conInput: {
    width: 300,
    backgroundColor: 'transparent',
    marginBottom: 0,
    borderRadius: 20,
  },
  conButon1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1589FF',
    width: 180,
    height: 50,
    borderWidth: 2,
    borderColor: '#555',
    marginBottom: 5,
    borderRadius: 25,
    margin: 3,
  },
  conButon2: {
    marginTop: 10,
    marginLeft: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#64E986',
    width: 150,
    height: 50,
    borderWidth: 2,
    borderColor: '#555',
    marginBottom: 5,
    borderRadius: 25,
    margin: 3,
  },
});
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     // backgroundColor: '#00a8ff',
//     width: '100%',
//   },
//   conLogin: {
//     flexDirection: 'row',
//     justifyContent: 'center',
//     borderWidth: 2,
//     backgroundColor: 'transparent',
//     borderRadius: 25,
//   },
//   conInput: {
//     width: 300,
//     backgroundColor: 'transparent',
//     marginBottom: 0,
//     borderRadius: 30,
//   },

// });
